package Sheridan;

import static org.junit.Assert.*;

import org.junit.Test;


public class FahrenheitTest {
	
	@Test
	public void testValidTempRgular()
	{
		int isValidConversion = Fahrenheit.fromCelsius(35);
		assertTrue("Invalid conversion", isValidConversion == 95);
	}
	

	@Test(expected= NumberFormatException.class)
	public void testValidTempException()
	{
		int isValidConversion = Fahrenheit.fromCelsius(25);
		assertTrue("Invalid conversion", isValidConversion == 77);
	}
	
	@Test
	public void testValidTempBoundaryIn()
	{
		int isValidConversion = Fahrenheit.fromCelsius(1);
		assertTrue("Invalid conversion", isValidConversion == 34);
	}
	
	@Test(expected= NumberFormatException.class)
	public void testValidTempBoundaryOut()
	{
		int isValidConversion = Fahrenheit.fromCelsius(-1);
		assertTrue("Invalid conversion", isValidConversion == 30);
	}
	
	

}
