package Sheridan;

public class Fahrenheit {
	
	public static  int fromCelsius(int tempC)
	{
		int fah = (tempC * (9/5)) + 32;
		return Math.round(fah);
	}
}
